var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET about page. */
router.get('/about', function(req, res, next) {
  res.render('about', { title: 'Express about' });
});

/* GET login page. */
router.get('/login', function(req, res){
  if(req.query.fail)
    res.render('login', { message: 'Usuário e/ou senha incorretos!' });
  else
    res.render('login', { message: null });
});

/* POST login page. */
router.post('/login',
  passport.authenticate('local', { successRedirect: '/contacts', failureRedirect: '/login?fail=true' })
);

module.exports = router;
