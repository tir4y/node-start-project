var express = require('express');
var router = express.Router();
var passport = require('passport');

function authenticationMiddleware () {  
  return function (req, res, next) {
    if (req.isAuthenticated()) {
      return next()
    }
    res.redirect('/login?fail=true')
  }
}
/* GET home page. */
router.get('/', authenticationMiddleware(), function(req, res, next) {
  var contactModel = require('../models/Contact');
  var Users = contactModel.Mongoose.model('contactscollection', contactModel.ContactSchema, 'contactscollection');
  Users.find({}).lean().exec(
     function (e, docs) {
        res.render('contacts/index', { title: 'Contatos', userlist: docs });
  });
});

/* GET about page. */
router.get('/about',authenticationMiddleware(), function(req, res, next) {
  res.render('about', { title: 'Express about' });
});



module.exports = router;
