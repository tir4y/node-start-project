var mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_CONNECTION);

var contactSchema = new mongoose.Schema({
    username: String,
    email: String
}, { collection: 'contactscollection' }
);

module.exports = { Mongoose: mongoose, ContactSchema: contactSchema }